<?php

/* ----------------------------------------------------------------------------------
Setup the various image sizes & thumbnail support
---------------------------------------------------------------------------------- */
	
	# Add post thumbnails (http://codex.wordpress.org/Post_Thumbnails)
	add_theme_support('post-thumbnails');
	
	// set_post_thumbnail_size(150, 150, false);
	// add_image_size('category-thumb', 300, 9999); // 300px wide (and unlimited height)