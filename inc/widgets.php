<?php

/* ----------------------------------------------------------------------------------
Register widgetized areas
---------------------------------------------------------------------------------- */

	function mhwp_widgets_init() {
		
		// Primary Sidebar
		register_sidebar(array(
			'name'          => __('Primary Sidebar', 'mhwp'),
			'id'            => 'sidebar-primary',
			'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></section>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>',
			)
		);
		
		// Footer Widget Area
		register_sidebar(array(
			'name'          => __('Footer 1', 'mhwp'),
			'id'            => 'footer1',
			'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></section>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>',
			)
		);
		
		// Footer Widget Area
		register_sidebar(array(
			'name'          => __('Footer 2', 'mhwp'),
			'id'            => 'footer2',
			'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></section>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>',
			)
		);
		
		// Footer Widget Area
		register_sidebar(array(
			'name'          => __('Footer 3', 'mhwp'),
			'id'            => 'footer3',
			'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></section>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>',
			)
		);
		
		// Footer Widget Area
		register_sidebar(array(
			'name'          => __('Footer 4', 'mhwp'),
			'id'            => 'footer4',
			'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></section>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>',
			)
		);

		// Register widgets
		register_widget('Volume9_Vcard_Widget');
	
	}
	
	add_action('widgets_init', 'mhwp_widgets_init');

/* ----------------------------------------------------------------------------------
Register Example VCard Widget
---------------------------------------------------------------------------------- */
	
	class Volume9_Vcard_Widget extends WP_Widget {
		
		private $fields = array(
			'title'          => 'Title (optional)',
			'street_address' => 'Street Address',
			'locality'       => 'City/Locality',
			'region'         => 'State/Region',
			'postal_code'    => 'Zipcode/Postal Code',
			'tel'            => 'Telephone',
			'email'          => 'Email'
		);

		function __construct() {
			
			$widget_ops = array(
				'classname' => 'widget_mhwp_vcard', 
				'description' => __('Use this widget to add a vCard', 'mhwp')
			);

			$this->WP_Widget('widget_mhwp_vcard', __('Volume9: vCard', 'mhwp'), $widget_ops);
			$this->alt_option_name = 'widget_mhwp_vcard';

			add_action('save_post', array(&$this, 'flush_widget_cache'));
			add_action('deleted_post', array(&$this, 'flush_widget_cache'));
			add_action('switch_theme', array(&$this, 'flush_widget_cache'));
		
		}

		function widget($args, $instance) {
			
			$cache = wp_cache_get('widget_mhwp_vcard', 'widget');

			if (!is_array($cache)) {
				
				$cache = array();
			
			}

			if (!isset($args['widget_id'])) {
				
				$args['widget_id'] = null;
			
			}

			if (isset($cache[$args['widget_id']])) {
				
				echo $cache[$args['widget_id']];
				return;
			
			}

			ob_start();
			extract($args, EXTR_SKIP);

			$title = apply_filters('widget_title', empty($instance['title']) ? __('vCard', 'mhwp') : $instance['title'], $instance, $this->id_base);

			foreach($this->fields as $name => $label) {
				
				if (!isset($instance[$name])) { $instance[$name] = ''; }
				
			}

			echo $before_widget;

			if ($title) {
				
				echo $before_title, $title, $after_title;
			
			}
		
			?>
			
			<p class="vcard">
				
				<a class="fn org url" href="<?php echo home_url('/'); ?>"><?php bloginfo('name'); ?></a><br>
				
				<span class="adr">
					
					<span class="street-address"><?php echo $instance['street_address']; ?></span><br>
					<span class="locality"><?php echo $instance['locality']; ?></span>,
					<span class="region"><?php echo $instance['region']; ?></span>
					<span class="postal-code"><?php echo $instance['postal_code']; ?></span><br>
				
				</span>
				
				<span class="tel"><span class="value"><?php echo $instance['tel']; ?></span></span><br>
				<a class="email" href="mailto:<?php echo $instance['email']; ?>"><?php echo $instance['email']; ?></a>
			
			</p>
			
			<?php
				
			echo $after_widget;

			$cache[$args['widget_id']] = ob_get_flush();
			wp_cache_set('widget_mhwp_vcard', $cache, 'widget');
				
		}

		function update($new_instance, $old_instance) {
				
			$instance = array_map('strip_tags', $new_instance);

			$this->flush_widget_cache();

			$alloptions = wp_cache_get('alloptions', 'options');
				
			if (isset($alloptions['widget_mhwp_vcard'])) {
					
				delete_option('widget_mhwp_vcard');
				
			}

			return $instance;
		
		}

		function flush_widget_cache() {
			
			wp_cache_delete('widget_mhwp_vcard', 'widget');
		
		}

		function form($instance) {
			
			foreach($this->fields as $name => $label) {
				
				${$name} = isset($instance[$name]) ? esc_attr($instance[$name]) : '';
			?>
			
				<p>
				
					<label for="<?php echo esc_attr($this->get_field_id($name)); ?>"><?php _e("{$label}:", 'mhwp'); ?></label>
					<input class="widefat" id="<?php echo esc_attr($this->get_field_id($name)); ?>" name="<?php echo esc_attr($this->get_field_name($name)); ?>" type="text" value="<?php echo ${$name}; ?>">
			
				</p>
				<?php
		
			}
	
		}

	} // End Widget