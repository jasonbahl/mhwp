<?php
/*
Plugin Name: Meta Box
Plugin URI: http://www.deluxeblogtips.com/meta-box
Description: Create meta box for editing pages in WordPress. Compatible with custom post types since WP 3.0
Version: 4.2.2
Author: Rilwis
Author URI: http://www.deluxeblogtips.com
License: GPL2+
*/

// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

// Script version, used to add version for scripts and styles
define( 'MHWP_VER', '4.2.2' );

// Define plugin URLs, for fast enqueuing scripts and styles
if ( ! defined( 'MHWPMETABOXES_URL' ) )
	define( 'MHWPMETABOXES_URL', plugin_dir_url( __FILE__ ) );
define( 'MHWP_JS_URL', trailingslashit( MHWPMETABOXES_URL . 'js' ) );
define( 'MHWP_CSS_URL', trailingslashit( MHWPMETABOXES_URL . 'css' ) );

// Plugin paths, for including files
if ( ! defined( 'MHWPMETABOXES_DIR' ) )
	define( 'MHWPMETABOXES_DIR', plugin_dir_path( __FILE__ ) );
define( 'MHWP_INC_DIR', trailingslashit( MHWPMETABOXES_DIR . 'inc' ) );
define( 'MHWP_FIELDS_DIR', trailingslashit( MHWP_INC_DIR . 'fields' ) );
define( 'MHWP_CLASSES_DIR', trailingslashit( MHWP_INC_DIR . 'classes' ) );

// Optimize code for loading plugin files ONLY on admin side
// @see http://www.deluxeblogtips.com/?p=345

// Helper function to retrieve meta value
require_once MHWP_INC_DIR . 'helpers.php';

if ( is_admin() )
{
	require_once MHWP_INC_DIR . 'common.php';

	// Field classes
	foreach ( glob( MHWP_FIELDS_DIR . '*.php' ) as $file )
	{
		require_once $file;
	}

	// Main file
	require_once MHWP_CLASSES_DIR . 'meta-box.php';
}