<?php
// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

// Make sure "select" field is loaded
require_once MHWP_FIELDS_DIR . 'select.php';

if ( !class_exists( 'MHWP_Select_Advanced_Field' ) )
{
	class MHWP_Select_Advanced_Field extends MHWP_Select_Field
	{
		/**
		 * Enqueue scripts and styles
		 *
		 * @return void
		 */
		static function admin_enqueue_scripts()
		{
			wp_enqueue_style( 'select2', MHWP_CSS_URL . 'select2/select2.css', array(), '3.2' );
			wp_enqueue_style( 'mhwp_hartman-select-advanced', MHWP_CSS_URL . 'select-advanced.css', array(), MHWP_VER );

			wp_register_script( 'select2', MHWP_JS_URL . 'select2/select2.min.js', array(), '3.2', true );
			wp_enqueue_script( 'mhwp_hartman-select-advanced', MHWP_JS_URL . 'select-advanced.js', array( 'select2' ), MHWP_VER, true );
		}

		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $meta
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html( $html, $meta, $field )
		{
			$html = sprintf(
				'<select class="mhwp_hartman-select-advanced" name="%s" id="%s"%s data-options="%s">',
				$field['field_name'],
				$field['id'],
				$field['multiple'] ? ' multiple="multiple"' : '',
				esc_attr( json_encode( $field['js_options'] ) )
			);
			if ( !empty( $field['js_options']['placeholder'] ) )
				$html .= '<option></option>';

			$option = '<option value="%s" %s>%s</option>';

			foreach ( $field['options'] as $value => $label )
			{
				$html .= sprintf(
					$option,
					$value,
					selected( in_array( $value, $meta ), true, false ),
					$label
				);
			}
			$html .= '</select>';

			return $html;
		}

		/**
		 * Normalize parameters for field
		 *
		 * @param array $field
		 *
		 * @return array
		 */
		static function normalize_field( $field )
		{
			$field = parent::normalize_field( $field );

			$field = wp_parse_args( $field, array(
				'js_options' => array(),
			) );

			$field['js_options'] = wp_parse_args( $field['js_options'], array(
				'allowClear'  => true,
				'width'       => 'resolve',
				'placeholder' => __( 'Select a value', 'mhwp_hartman' )
			) );

			return $field;
		}
	}
}