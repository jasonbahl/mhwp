<?php
// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

if ( !class_exists( 'MHWP_Map_Field' ) )
{
	class MHWP_Map_Field
	{
		/**
		 * Enqueue scripts and styles
		 *
		 * @return void
		 */
		static function admin_enqueue_scripts()
		{
			wp_enqueue_script( 'googlemap', 'http://maps.google.com/maps/api/js?sensor=false', array(), '', true );
			wp_enqueue_script( 'mhwp_hartman-map', MHWP_JS_URL . 'map.js', array( 'jquery', 'googlemap' ), MHWP_VER, true );
		}

		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $meta
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html( $html, $meta, $field )
		{
			$address = isset( $field['address_field'] ) ? $field['address_field'] : false;

			$html = sprintf(
				'<div class="mhwp_hartman-map-canvas" style="%s"></div>
				<input type="hidden" name="%s" id="mhwp_hartman-map-coordinate" value="%s" />',
				isset( $field['style'] ) ? $field['style'] : '',
				$field['field_name'],
				$meta
			);

			if ( $address )
			{
				$html .= sprintf(
					'<button class="button" type="button" id="mhwp_hartman-map-goto-address-button" value="%s" onclick="geocodeAddress(this.value);">%s</button>',
					is_array( $address ) ? implode( ',', $address ) : $address,
					__( 'Find Address', 'mhwp_hartman' )
				);
			}
			return $html;
		}
	}
}