/**
 * Update color picker element
 * Used for static & dynamic added elements (when clone)
 */
function mhwp_hartman_update_color_picker()
{
	var $ = jQuery;
	$( '.mhwp_hartman-color-picker' ).each( function()
	{
		var $this = $( this ),
			$input = $this.siblings( 'input.mhwp_hartman-color' );

		// Make sure the value is displayed
		if ( !$input.val() )
			$input.val( '#' );

		$this.farbtastic( $input );
	} );
}

jQuery( document ).ready( function( $ )
{
	$( '.mhwp_hartman-input' ).delegate( '.mhwp_hartman-color', 'focus', function()
	{
		$( this ).siblings( '.mhwp_hartman-color-picker' ).show();
		return false;
	} ).delegate( '.mhwp_hartman-color', 'blur', function()
	{
		$( this ).siblings( '.mhwp_hartman-color-picker' ).hide();
		return false;
	} );

	mhwp_hartman_update_color_picker();
} );