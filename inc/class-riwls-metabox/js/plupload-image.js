jQuery( document ).ready( function( $ )
{
	// Object containing all the plupload uploaders
	var mhwp_hartman_image_uploaders = {},
		max;

	// Hide "Uploaded files" title as long as there are no files uploaded
	// Note that we can have multiple upload forms in the page, so relative path to current element is important
	$( '.mhwp_hartman-uploaded' ).each( function()
	{
		var $this = $( this ),
			$lis = $this.children(),
			$title = $this.siblings( '.mhwp_hartman-uploaded-title' );
		if ( 0 == $lis.length )
		{
			$title.addClass( 'hidden' );
			$this.addClass( 'hidden' );
		}
	} );

	// Hide "Uploaded files" title if there are no files uploaded after deleting files
	$( '.mhwp_hartman-images' ).on( 'click', '.mhwp_hartman-delete-file', function()
	{
		// Check if we need to show drop target
		var $images = $( this ).parents( '.mhwp_hartman-images' ),
			uploaded = $images.children().length - 1, // -1 for the one we just deleted
			$dragndrop = $images.siblings( '.mhwp_hartman-drag-drop' );

		if ( 0 == uploaded )
		{
			$images.siblings( '.mhwp_hartman-uploaded-title' ).addClass( 'hidden' );
			$images.addClass( 'hidden' );
		}

		// After delete files, show the Drag & Drop section
		$dragndrop.show();
	} );

	// Using all the image prefixes
	$( 'input:hidden.mhwp_hartman-image-prefix' ).each( function()
	{
		var prefix = $( this ).val(),
			nonce = $( '#nonce-upload-images_' + prefix ).val();

		// Adding container, browser button and drag ang drop area
		mhwp_hartman_plupload_init = $.extend( {
			container    : prefix + '-container',
			browse_button: prefix + '-browse-button',
			drop_element : prefix + '-dragdrop'
		}, mhwp_hartman_plupload_defaults );

		// Add field_id to the ajax call
		mhwp_hartman_plupload_init['multipart_params'] = {
			action  : 'mhwp_hartman_plupload_image_upload',
			field_id: prefix,
			_wpnonce: nonce,
			post_id : $( '#post_ID' ).val(),
			force_delete: $( this ).data( 'force_delete' )
		};

		// Create new uploader
		mhwp_hartman_image_uploaders[prefix] = new plupload.Uploader( mhwp_hartman_plupload_init );
		mhwp_hartman_image_uploaders[prefix].init();

		mhwp_hartman_image_uploaders[prefix].bind( 'FilesAdded', function( up, files )
		{
			var max_file_uploads = $( '#' + this.settings.container + ' .max_file_uploads' ).val(),
				uploaded = $( '#' + this.settings.container + ' .mhwp_hartman-uploaded' ).children().length,
				msg = 'You may only upload ' + max_file_uploads + ' file';

			if ( max_file_uploads > 1 )
				msg += 's';

			// Remove files from queue if exceed max file uploads
			if ( ( uploaded + files.length ) > max_file_uploads )
			{
				for ( var i = files.length; i--; )
				{
					up.removeFile( files[i] );
				}
				alert( msg );
				return false;
			}

			// Hide drag & drop section if reach max file uploads
			if ( ( uploaded + files.length ) == max_file_uploads )
				$( '#' + this.settings.container ).find( '.mhwp_hartman-drag-drop' ).hide();

			max = parseInt( up.settings.max_file_size, 10 );

			// Upload files
			plupload.each( files, function( file )
			{
				add_loading( up, file );
				add_throbber( file );
				if ( file.size >= max )
					remove_error( file );
			} );
			up.refresh();
			up.start();
		} );

		mhwp_hartman_image_uploaders[prefix].bind( 'Error', function( up, e )
		{
			add_loading( up, e.file );
			remove_error( e.file );
			up.removeFile( e.file );
		} );

		mhwp_hartman_image_uploaders[prefix].bind( 'UploadProgress', function( up, file )
		{
			var $uploaded = $( '#' + this.settings.container + ' .mhwp_hartman-uploaded' ),
				$uploaded_title = $( '#' + this.settings.container + ' .mhwp_hartman-uploaded-title' );

			// Update the loading div
			$( 'div.mhwp_hartman-image-uploading-bar', 'li#' + file.id ).css( 'height', file.percent + '%' );

			// Show them all
			$uploaded.removeClass( 'hidden' );
			$uploaded_title.removeClass( 'hidden' );
		} );

		mhwp_hartman_image_uploaders[prefix].bind( 'FileUploaded', function( up, file, response )
		{
			var res = wpAjax.parseAjaxResponse( $.parseXML( response.response ), 'ajax-response' );
			false === res.errors ? $( 'li#' + file.id ).replaceWith( res.responses[0].data ) : remove_error( file );
		} );
	} );

	/**
	 * Helper functions
	 */

	/**
	 * Removes li element if there is an error with the file
	 *
	 * @return void
	 */
	function remove_error( file )
	{
		$( 'li#' + file.id )
			.addClass( 'mhwp_hartman-image-error' )
			.delay( 1600 )
			.fadeOut( 'slow', function()
			{
				$( this ).remove();
			}
		);
	}

	/**
	 * Adds loading li element
	 *
	 * @return void
	 */
	function add_loading( up, file )
	{
		$list = $( '#' + up.settings.container ).find( 'ul' );
		$list.append( "<li id='" + file.id + "'><div class='mhwp_hartman-image-uploading-bar'></div><div id='" + file.id + "-throbber' class='mhwp_hartman-image-uploading-status'></div></li>" );
	}

	/**
	 * Adds loading throbber while waiting for a response
	 *
	 * @return void
	 */
	function add_throbber( file )
	{
		$( '#' + file.id + '-throbber' ).html( "<img class='mhwp_hartman-loader' height='64' width='64' src='" + RWMB.url + "img/loader.gif'/>" );
	}
} );
