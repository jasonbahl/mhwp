/**
 * Update date picker element
 * Used for static & dynamic added elements (when clone)
 */
function mhwp_hartman_update_date_picker()
{
	var $ = jQuery;

	$( '.mhwp_hartman-date' ).each( function()
	{
		var $this = $( this ),
			options = $this.data( 'options' );

		$this.siblings( '.ui-datepicker-append' ).remove();         // Remove appended text
		$this.removeClass( 'hasDatepicker' ).datepicker( options );
	} );
}

jQuery( document ).ready( function()
{
	mhwp_hartman_update_date_picker();
} );