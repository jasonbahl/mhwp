/**
 * Update select2
 * Used for static & dynamic added elements (when clone)
 */
function mhwp_hartman_update_select_advanced()
{
	var $ = jQuery;

	$( '.mhwp_hartman-select-advanced' ).each( function ()
	{
		var $this = $( this ),
			options = $this.data( 'options' );
		$this.select2( options );
	} );
}

jQuery( document ).ready( function ()
{
	mhwp_hartman_update_select_advanced();
} );