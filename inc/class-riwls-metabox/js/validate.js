jQuery( document ).ready( function( $ )
{
	var $form = $( '#post' );

	// Required field styling
	$.each( mhwp_hartman.validationOptions.rules, function( k, v )
	{
		if ( v['required'] )
			$( '#' + k ).parent().siblings( '.mhwp_hartman-label' ).addClass( 'required' ).append( '<span>*</span>' );
	} );

	mhwp_hartman.validationOptions.invalidHandler = function( form, validator )
	{
		// Re-enable the submit ( publish/update ) button and hide the ajax indicator
		$( '#publish' ).removeClass( 'button-primary-disabled' );
		$( '#ajax-loading' ).attr( 'style', '' );
		$form.siblings( '#message' ).remove();
		$form.before( '<div id="message" class="error"><p>' + mhwp_hartman.summaryMessage + '</p></div>' );
	};

	$form.validate( mhwp_hartman.validationOptions );
} );