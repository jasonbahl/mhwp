/**
 * Update datetime picker element
 * Used for static & dynamic added elements (when clone)
 */
function mhwp_hartman_update_datetime_picker()
{
	var $ = jQuery;

	$( '.mhwp_hartman-datetime' ).each( function()
	{
		var $this = $( this ),
			options = $this.data( 'options' );

		$this.siblings( '.ui-datepicker-append' ).remove();         // Remove appended text
		$this.removeClass( 'hasDatepicker' ).datetimepicker( options );
	} );
}

jQuery( document ).ready( function()
{
	mhwp_hartman_update_datetime_picker();
} );