<?php

/* ----------------------------------------------------------------------------------
Enable Theme features
---------------------------------------------------------------------------------- */
	
	add_theme_support('rewrite-urls');          // Enable URL rewrites
	add_theme_support('h5bp-htaccess');         // Enable HTML5 Boilerplate's .htaccess
	
	add_theme_support('post-thumbnails');		// Enable Featured Thumbnails
	
	# set_post_thumbnail_size(150, 150, false);
	# add_image_size('category-thumb', 300, 9999); // 300px wide (and unlimited height)

/* ----------------------------------------------------------------------------------
Add Support For Post Formats
---------------------------------------------------------------------------------- */

	// Add post formats (http://codex.wordpress.org/Post_Formats)
	add_theme_support('post-formats', array(
		'aside', 
		'gallery', 
		'link', 
		'image', 
		'quote', 
		'status', 
		'video', 
		'audio', 
		'chat'
		)
	);

/* ----------------------------------------------------------------------------------
Register the Nav Menus
---------------------------------------------------------------------------------- */
	
	function mhwp_register_nav_menus() {
	
		register_nav_menus(
			array(
				'primary_navigation' => __('Primary Navigation', 'mhwp'),
			)
		);
	
	}
	
	add_action('init', 'mhwp_register_nav_menus');

/* ----------------------------------------------------------------------------
	Create some JS Variables
---------------------------------------------------------------------------- */

	function mhwp_create_reusable_js_variables() {

		$child_dir		= get_bloginfo('stylesheet_directory');
		$template_dir	= get_bloginfo('template_url');
		$permalink		= get_permalink();
		$siteRoot		= get_bloginfo('url');
		$ajaxurl		= admin_url('admin-ajax.php');
		$adminRoot		= admin_url();

		echo '
		<script type="text/javascript">
			var childDir	= "'.$child_dir.'";
			var templateDir = "'.$template_dir.'";
			var siteRoot 	= "'.$siteRoot.'";
			var adminRoot 	= "'.$adminRoot.'";
			var ajaxurl 	= "'.$ajaxurl.'";
		</script>
		';

	}

	add_action('wp_head','mhwp_create_reusable_js_variables', 999);
	add_action('admin_head','mhwp_create_reusable_js_variables');

/* ----------------------------------------------------------------------------------
 * Define which pages / page templates shouldn't have the sidebar
 *
 * See lib/sidebar.php for more details
---------------------------------------------------------------------------------- */

	function mhwp_display_sidebar() {

		$sidebar_config = new Volume9_Sidebar(

		    /**
		     * Conditional tag checks (http://codex.wordpress.org/Conditional_Tags)
		     * Any of these conditional tags that return true won't show the sidebar
		     *
		     * To use a function that accepts arguments, use the following format:
		     *
		     * array('function_name', array('arg1', 'arg2'))
		     *
		     * The second element must be an array even if there's only 1 argument.
		     */

		     array(
		     	'is_404',
		     	'is_front_page'
		     ),

		     /**
			 * Page template checks (via is_page_template())
			 * Any of these page templates that return true won't show the sidebar
			 */

			 array(
			 	'page-full-width.php'
			 )

		 );

		 return $sidebar_config->display;
	 }

/* ----------------------------------------------------------------------------------
#main CSS classes
---------------------------------------------------------------------------------- */

	function mhwp_main_class() {

		if (mhwp_display_sidebar()) {

<<<<<<< HEAD
			$class = 'col-8';

		} else {

			$class = 'col-12';
=======
			$class = 'col-md-8';

		} else {

			$class = 'col-md-12';
>>>>>>> 175057a145d727f5260fa72bec52faf7a485b18e

		}

		return $class;

	}

/* ----------------------------------------------------------------------------------
#sidebar CSS classes
---------------------------------------------------------------------------------- */

	function mhwp_sidebar_class() {

<<<<<<< HEAD
		return 'col-4';
=======
		return 'col-md-4';
>>>>>>> 175057a145d727f5260fa72bec52faf7a485b18e

	}

/* ----------------------------------------------------------------------------------
* $content_width is a global variable used by WordPress for max image upload sizes and media embeds (in pixels)
*
* Example: If the content area is 640px wide, set $content_width = 620; so images and videos will not overflow.
*
* Default: 940px is the default Bootstrap container width.
*
* This is not required or used by Volume9.
---------------------------------------------------------------------------------- */

	if (!isset($content_width)) {
		$content_width = 940;
	}
	
/* ---------------------------------------------------------------------------- */
/* Disable Visual Composer Stylesheet
/* ---------------------------------------------------------------------------- */
	
	add_action( 'wp_enqueue_scripts', 'mhwp_dequeue_visual_composer_styles', 20 );
	
	function mhwp_dequeue_visual_composer_styles(){
    
    	wp_deregister_style( 'js_composer_front' );
	
	}

// -------------------------------------------------------------------------- //
// Add Full Width Page Header
// -------------------------------------------------------------------------- //

	add_action('mhwp_before_wrapper', 'mhwp_fullwidth_header');

	function mhwp_fullwidth_header() {

		if (!is_front_page()) {

			$output = '<div id="fullwidth-header">';
				
				if( function_exists('bcn_display') ) {
        	
		        	if ( !is_front_page() ) {
		        		
		        		if(function_exists('bcn_display')) {

		        			$output .= '<div class="breadcrumbContainer">';
								$output .= '<div class="breadcrumb">';
									$output .= '<a title="Go to '.get_bloginfo('name').'." href="'.get_bloginfo('url').'" class="fa fa-home"><i class="icon-home"></i></a> > ' . bcn_display(true);
								$output .= '</div>';	
							$output .= '</div>';
		        		
		        		}
		        		
		        	}
		        	
		        }
				
				$output .= '<div class="container">';
					$output .= '<div class="row-fluid">';
						$output .= '<div class="headerInfo wpb_right-to-left wpb_left-to-right wpb_start_animation">';
							$output .= apply_filters( 'mhwp_fullwidth_header_text', '<h1>'.mhwp_page_title().'</h1>' );
							$output .= '<br/>';
							$output .= '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat</p>';
						$output .= '</div>';
					$output .= '</div>';
				$output .= '</div>';
			$output .= '</div>';

		}
		
		$output =& $output;
		
		echo apply_filters('mhwp_fullwidth_header', $output);

	}
	
// -------------------------------------------------------------------------- //
// Add Slideshow to homepage
// -------------------------------------------------------------------------- //

	add_action( 'mhwp_before_wrapper', 'mhwp_home_slideshow' );
	
	function mhwp_home_slideshow() {
		
		$output = '';
		
		if (is_front_page() && function_exists( putRevSlider('home') ) ) {
			
			$output .= '<div class="home-featured">';
			
				$output .= putRevSlider('home');
			
			$output .= '</div>';
			
		}
		
		echo $output;
		
	}
	
// -------------------------------------------------------------------------- //
// Add Top Header Bar
// -------------------------------------------------------------------------- //

	add_action( 'mhwp_before_banner_header_container', 'mhwp_top_header_bar' );
	
	function mhwp_top_header_bar() {
		
		$output = '';
		
		$output .= '<div class="topHeader">';
			
			$output .= '<div class="container">';
				
				$output .= '<div class="container-inner">';
				
					$output .= 'Demo Request | Call: (855)-433-7133 | Login';
				
				$output .= '</div>';
			
			$output .= '</div>';
			
		$output .= '</div>';
		
		echo $output;
		
	}

/* ---------------------------------------------------------------------------- */
/* Disable Visual Composer Stylesheet
/* ---------------------------------------------------------------------------- */
	
	add_action( 'wp_enqueue_scripts', 'mhwp_dequeue_js_composer_styles', 20 );
	
	function mhwp_dequeue_js_composer_styles(){
    
    	wp_deregister_style( 'js_composer_front' );
    	wp_deregister_style( 'vc_plugin_themes_css' );
	
	}

/* ---------------------------------------------------------------------------- */
/* Sub-Menu
/* ---------------------------------------------------------------------------- */

	function mhwp_sub_menu() {
		
		// Get Sub-Menu
		$show_sub_menu = get_field( 'show_sub_menu' );
		$sub_menu = get_field( 'sub_menu' );
		
		$menu_slug = $sub_menu->slug;
		
		$output = '';
		
		if ( $show_sub_menu ) {
		
			if ( $menu_slug ) {

				$output .= wp_nav_menu(
					array(
						'menu' 				=> $menu_slug,
						'menu_class' 		=> 'mhwp-sub-menu',
						'menu_id' 			=> 'mhwp-sub-menu',
						'echo'				=> false,
						'container_class'	=> 'mhwp-sub-menu-wrapper'
					)
				);
				
			}
			
		}
		
		echo $output;
		
	}
	
	add_action( 'mhwp_before_wrapper', 'mhwp_sub_menu' );
