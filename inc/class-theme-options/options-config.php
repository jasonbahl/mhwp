<?php

/* ---------------------------------------------------------------------------- */
/* Set the text domain for the theme or plugin.
/* ---------------------------------------------------------------------------- */
define('MHWP_Options', 'mhwp-opts');

/* ---------------------------------------------------------------------------- */
/* Require the Options Panel Class
/* ---------------------------------------------------------------------------- */
if(!class_exists('MHWP_Options')){
    require_once(dirname(__FILE__) . '/options/options.php');
}

/* ---------------------------------------------------------------------------- */
/* Add Another Panel Via a Filter
/* ---------------------------------------------------------------------------- */

	function add_another_section($sections){
	    
	    $sections[] = array(
	        'title' => __('A Section added by hook', MHWP_TEXT_DOMAIN),
	        'desc' 	=> __('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', MHWP_TEXT_DOMAIN),
	        'icon' => MHWP_OPTIONS_URL . 'img/icons/glyphicons_008_film.png',
	        'fields' => array()
	    );
	
	    return $sections;
	
	}
	
	add_filter('mhwp-opts-sections-mhwp', 'add_another_section');

/*
 *
 * Most of your editing will be done in this section.
 *
 * Here you can override default values, uncomment args and change their values.
 * No $args are required, but they can be over ridden if needed.
 *
 */
 
/* ---------------------------------------------------------------------------- */
/* Setup Default Framework Sections
/* ---------------------------------------------------------------------------- */
	
	add_action('init', 'setup_framework_options', 0);
	
	function setup_framework_options(){
	    
	    $args = array();
	
	    $args['dev_mode'] 				= false;
	    $args['google_api_key'] 		= 'AIzaSyCIxnEtlyhwLcsfIe0U5xLvew2b1C9aijU';
	    $args['admin_stylesheet'] 		= 'standard';
	    $args['show_import_export'] 	= false;
	    $args['opt_name'] 				= 'mhwp';
	    $args['menu_title'] 			= __('Theme Options', MHWP_TEXT_DOMAIN);
	    $args['page_title'] 			= __('Theme Options', MHWP_TEXT_DOMAIN);
	    $args['page_slug']	 			= 'theme_options';
	    $args['allow_sub_menu'] 		= true;
	    	
	    $sections = array();
		
	    $sections[] = array(
	        'icon' 		=> MHWP_OPTIONS_URL . 'img/icons/glyphicons_107_text_resize.png',
	        'title' 	=> __('General Settings', MHWP_TEXT_DOMAIN),
	        'desc' 		=> __('<p class="description">General Settings for the MHWP Framework.</p>', MHWP_TEXT_DOMAIN),
	        'fields' 	=> array(
	            
	            // Built-in field types include:
	            // text, textarea, editor, checkbox, multi_checkbox, radio, radio_img, button_set,
	            // select, multi_select, color, date, divide, info, upload
	            
	            // Built-in validation includes: 
	            //  email, html, html_custom, no_html, js, numeric, comma_numeric, url, str_replace, preg_replace
	            
	            array(
	                'id' 		=> '_logo',
	                'type' 		=> 'upload',
	                'title' 	=> __('Logo', MHWP_TEXT_DOMAIN),
	                'sub_desc' 	=> __('Upload Logo.', MHWP_TEXT_DOMAIN),
		            'desc' => __('', MHWP_TEXT_DOMAIN),
	            ),
	        )
	    );
	    
	    // $tabs[] = array();
	
	    global $MHWP_Options;
	    $MHWP_Options = new MHWP_Options($sections, $args);
	
	}