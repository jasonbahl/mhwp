/*global jQuery*/
/*
 *
 * MHWP_Options_radio_img function
 * Changes the radio select option, and changes class on images
 *
 */
function mhwp_radio_img_select(relid, labelclass) {
    jQuery(this).prev('input[type="radio"]').prop('checked');
    jQuery('.mhwp-radio-img-' + labelclass).removeClass('mhwp-radio-img-selected');
    jQuery('label[for="' + relid + '"]').addClass('mhwp-radio-img-selected');
}
