/*global jQuery, document*/
jQuery(document).ready(function () {
    /*
     *
     * MHWP_Options_date function
     * Adds datepicker js
     *
     */
    jQuery('.mhwp-opts-datepicker').datepicker();
});
