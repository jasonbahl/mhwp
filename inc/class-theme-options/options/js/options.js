/*global $, jQuery, document, tabid:true, mhwp_opts, confirm, relid:true*/

jQuery(document).ready(function () {

    if (jQuery('#last_tab').val() === '') {
        jQuery('.mhwp-opts-group-tab:first').slideDown('fast');
        jQuery('#mhwp-opts-group-menu li:first').addClass('active');
    } else {
        tabid = jQuery('#last_tab').val();
        jQuery('#' + tabid + '_section_group').slideDown('fast');
        jQuery('#' + tabid + '_section_group_li').addClass('active');
    }

    jQuery('input[name="' + mhwp_opts.opt_name + '[defaults]"]').click(function () {
        if (!confirm(mhwp_opts.reset_confirm)) {
            return false;
        }
    });

    jQuery('.mhwp-opts-group-tab-link-a').click(function () {
        relid = jQuery(this).attr('data-rel');

        jQuery('#last_tab').val(relid);

        jQuery('.mhwp-opts-group-tab').each(function () {
            if (jQuery(this).attr('id') === relid + '_section_group') {
                jQuery(this).delay(400).fadeIn(1200);
            } else {
                jQuery(this).fadeOut('fast');
            }
        });

        jQuery('.mhwp-opts-group-tab-link-li').each(function () {
            if (jQuery(this).attr('id') !== relid + '_section_group_li' && jQuery(this).hasClass('active')) {
                jQuery(this).removeClass('active');
            }
            if (jQuery(this).attr('id') === relid + '_section_group_li') {
                jQuery(this).addClass('active');
            }
        });
    });

    if (jQuery('#mhwp-opts-save').is(':visible')) {
        jQuery('#mhwp-opts-save').delay(4000).slideUp('slow');
    }

    if (jQuery('#mhwp-opts-imported').is(':visible')) {
        jQuery('#mhwp-opts-imported').delay(4000).slideUp('slow');
    }

    jQuery('input, textarea, select').change(function () {
        jQuery('#mhwp-opts-save-warn').slideDown('slow');
    });

    jQuery('#mhwp-opts-import-code-button').click(function () {
        if (jQuery('#mhwp-opts-import-link-wrapper').is(':visible')) {
            jQuery('#mhwp-opts-import-link-wrapper').fadeOut('fast');
            jQuery('#import-link-value').val('');
        }
        jQuery('#mhwp-opts-import-code-wrapper').fadeIn('slow');
    });

    jQuery('#mhwp-opts-import-link-button').click(function () {
        if (jQuery('#mhwp-opts-import-code-wrapper').is(':visible')) {
            jQuery('#mhwp-opts-import-code-wrapper').fadeOut('fast');
            jQuery('#import-code-value').val('');
        }
        jQuery('#mhwp-opts-import-link-wrapper').fadeIn('slow');
    });

    jQuery('#mhwp-opts-export-code-copy').click(function () {
        if (jQuery('#mhwp-opts-export-link-value').is(':visible')) {jQuery('#mhwp-opts-export-link-value').fadeOut('slow'); }
        jQuery('#mhwp-opts-export-code').toggle('fade');
    });

    jQuery('#mhwp-opts-export-link').click(function () {
        if (jQuery('#mhwp-opts-export-code').is(':visible')) {jQuery('#mhwp-opts-export-code').fadeOut('slow'); }
        jQuery('#mhwp-opts-export-link-value').toggle('fade');
    });
});
