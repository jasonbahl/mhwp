<?php

/* ----------------------------------------------------------------------------------
Volume9 initial setup and constants
---------------------------------------------------------------------------------- */
	
	function mhwp_setup() {

		// Make theme available for translation
		load_theme_textdomain('mhwp', get_template_directory() . '/lang');

		// Tell the TinyMCE editor to use a custom stylesheet
		add_editor_style('assets/css/editor-style.css');

	}

	add_action('init', 'mhwp_setup');

/* ----------------------------------------------------------------------------------
Backwards compatibility for older than PHP 5.3.0
---------------------------------------------------------------------------------- */
	
	if (!defined('__DIR__')) { 
		define('__DIR__', dirname(__FILE__)); 
	}

/* ----------------------------------------------------------------------------------
Theme Wrapper
@link http://scribu.net/wordpress/theme-wrappers.html
---------------------------------------------------------------------------------- */

	function mhwp_template_path() {
		
		return Volume9_Wrapping::$main_template;
	
	}

	class Volume9_Wrapping {

		// Stores the full path to the main template file
		static $main_template;

		// Stores the base name of the template file; e.g. 'page' for 'page.php' etc.
		static $base;

		static function wrap($template) {
			
			self::$main_template = $template;

			self::$base = substr(basename(self::$main_template), 0, -4);

			if (self::$base === 'index') {
				
				self::$base = false;
			
			}

			$templates = array('base.php');

			if (self::$base) {
				
				array_unshift($templates, sprintf('base-%s.php', self::$base ));
			
			}

			return locate_template($templates);
	
		}
	
	}

	add_filter('template_include', array('Volume9_Wrapping', 'wrap'), 99);

/* ----------------------------------------------------------------------------------
Returns WordPress subdirectory if applicable
---------------------------------------------------------------------------------- */

	function wp_base_dir() {
		
		preg_match('!(https?://[^/|"]+)([^"]+)?!', site_url(), $matches);
		
		if (count($matches) === 3) {
	
			return end($matches);
	
		} else {
	
			return '';
	
		}
	
	}

	// Opposite of built in WP functions for trailing slashes
	function leadingslashit($string) {
		
		return '/' . unleadingslashit($string);
	
	}

	function unleadingslashit($string) {
		
		return ltrim($string, '/');
	
	}

	function add_filters($tags, $function) {
		
		foreach($tags as $tag) {
			
			add_filter($tag, $function);
		
		}
	
	}

	function is_element_empty($element) {
		
		$element = trim($element);
		return empty($element) ? false : true;
	
	}

/* ----------------------------------------------------------------------------------
* URL rewriting and addition of HTML5 Boilerplate's .htaccess
*
* Rewrites currently do not happen for child themes (or network installs)
* @todo https://github.com/retlehs/roots/issues/461
*
* Rewrite:
*   /wp-content/themes/themename/css/ to /css/
*   /wp-content/themes/themename/js/  to /js/
*   /wp-content/themes/themename/img/ to /img/
*   /wp-content/plugins/              to /plugins/
*
* If you aren't using Apache, alternate configuration settings can be found in the wiki.
*
* @link https://github.com/retlehs/roots/wiki/Nginx
* @link https://github.com/retlehs/roots/wiki/Lighttpd
---------------------------------------------------------------------------------- */
	
	if (stristr($_SERVER['SERVER_SOFTWARE'], 'apache') || stristr($_SERVER['SERVER_SOFTWARE'], 'litespeed') !== false) {

		// Show an admin notice if .htaccess isn't writable
		function mhwp_htaccess_writable() {
			
			if (!is_writable(get_home_path() . '.htaccess')) {
				
				if (current_user_can('administrator')) {
					
					add_action('admin_notices', create_function('', "echo '<div class=\"error\"><p>" . sprintf(__('Please make sure your <a href="%s">.htaccess</a> file is writable ', 'mhwp'), admin_url('options-permalink.php')) . "</p></div>';"));
			
				}
			
			}
		
		}

		add_action('admin_init', 'mhwp_htaccess_writable');


		// Add rewrites
		function mhwp_add_rewrites($content) {

		    global $wp_rewrite;
		    
		    $mhwp_new_non_wp_rules = array(
		    	'assets/css/(.*)'      => THEME_PATH . '/assets/css/$1',
		    	'assets/js/(.*)'       => THEME_PATH . '/assets/js/$1',
		    	'assets/img/(.*)'      => THEME_PATH . '/assets/img/$1',
		    	'plugins/(.*)'  => RELATIVE_PLUGIN_PATH . '/$1'
		    );
    
		    $wp_rewrite->non_wp_rules = array_merge($wp_rewrite->non_wp_rules, $mhwp_new_non_wp_rules);
		    return $content;
		}

		
		// Clean URLs
		function mhwp_clean_urls($content) {
			
			if (strpos($content, FULL_RELATIVE_PLUGIN_PATH) === 0) {
				
				return str_replace(FULL_RELATIVE_PLUGIN_PATH, WP_BASE . '/plugins', $content);
			
			} else {
      
				return str_replace('/' . THEME_PATH, '', $content);
			
			}
		
		}

		
		// Permalink Structure
		if (!is_multisite() && !is_child_theme() && get_option('permalink_structure')) {
			
			if (current_theme_supports('rewrite-urls')) {
				
				add_action('generate_rewrite_rules', 'mhwp_add_rewrites');
			
			}

			if (current_theme_supports('h5bp-htaccess')) {
				
				add_action('generate_rewrite_rules', 'mhwp_add_h5bp_htaccess');
			
			}

			if (!is_admin() && current_theme_supports('rewrite-urls')) {
				
				$tags = array(
					'plugins_url',
					'bloginfo',
					'stylesheet_directory_uri',
					'template_directory_uri',
					'script_loader_src',
					'style_loader_src'
				);

				add_filters($tags, 'mhwp_clean_urls');
			
			}
		
		}

		
		
		// Add the contents of h5bp-htaccess into the .htaccess file
		function mhwp_add_h5bp_htaccess($content) {
			
			global $wp_rewrite;
			$home_path = function_exists('get_home_path') ? get_home_path() : ABSPATH;
			$htaccess_file = $home_path . '.htaccess';
			$mod_rewrite_enabled = function_exists('got_mod_rewrite') ? got_mod_rewrite() : false;

			if ((!file_exists($htaccess_file) && is_writable($home_path) && $wp_rewrite->using_mod_rewrite_permalinks()) || is_writable($htaccess_file)) {
				
				if ($mod_rewrite_enabled) {
					
					$h5bp_rules = extract_from_markers($htaccess_file, 'HTML5 Boilerplate');
					
					if ($h5bp_rules === array()) {
						
						$filename = dirname(__FILE__) . '/h5bp-htaccess';
						return insert_with_markers($htaccess_file, 'HTML5 Boilerplate', extract_from_markers($filename, 'HTML5 Boilerplate'));
					
					}
				
				}
			
			}

			return $content;
		
		}
	
	}

/* ----------------------------------------------------------------------------------
Dynamic Logo
---------------------------------------------------------------------------------- */

	function mhwp_logo() {
		
		global $mhwp_options;
		
		// Get Uploaded Logo
		$image_logo 	= $mhwp_options['_logo'];
		
		// Uncomment for testing
		// print_r($uploaded_logo);
		
		if ($image_logo) {
			
			$output = '<div class="headerLogo">';
				
				$output .= '<a href="'.get_bloginfo('url').'"><img src="'.$image_logo.'" alt="'.get_bloginfo('name').' Logo" class="logo"></a>';
			
			$output .= '</div>';

		} else {
			
			// Output Site Title
			$output = '<h2 class="site-title">'.get_bloginfo('name').'</h2>';
			
		}
		
		echo apply_filters('mhwp_logo', $output);
		
	}

/* ----------------------------------------------------------------------------------
Icons Select
---------------------------------------------------------------------------------- */	

	function get_mhwp_icon_select( $name, $value ) {
		
	
		#To save a ton of work we're going to parse the font-awesome icons and generate them on the fly
		$pattern = '/\.(icon-(?:\w+(?:-)?)+):before\s+{\s*content:\s*"(.+)";\s+}/';
		$subject = file_get_contents(MHWPPATH . '/components/font-awesome/css/font-awesome.css');
		preg_match_all($pattern, $subject, $matches, PREG_SET_ORDER);
		sort($matches);
	
		#Select Field
		$output = '<select class="widefat code edit-menu-icon" name="'.$name.'">';
			
			$output .=' <option value="no-icon">No Icon</option>';
			
			foreach($matches as $match){
			    
			    if ($value == $match[1]) {
				    $selected = ' selected="selected"';
			    } else {
				    $selected = '';
			    }
			    
			    $output .= '<option data-imagesrc="'.$match[1].'" value="'.$match[1].'" '.$selected.'>'.ucfirst(str_replace('icon-', NULL, $match[1])).'</option>';
			
			}
	
	
		#End Select Field
		$output .= '</select>';
	
		return apply_filters('get_mhwp_icon_select', $output);
	
	}
	
	function mhwp_icon_select( $name, $value ) {
		
		echo get_mhwp_icon_select( $name, $value );
		
	}
	
/* ----------------------------------------------------------------------------------

Determines whether or not to display the sidebar based on an array of conditional tags or page templates.

If any of the is_* conditional tags or is_page_template(template_file) checks return true, the sidebar will NOT be displayed.

@param array list of conditional tags (http://codex.wordpress.org/Conditional_Tags)
@param array list of page templates. These will be checked via is_page_template()

@return boolean True will display the sidebar, False will not

---------------------------------------------------------------------------------- */

	class Volume9_Sidebar {
		
		private $conditionals;
		private $templates;

		public $display = true;

		function __construct($conditionals = array(), $templates = array()) {
			
			$this->conditionals = $conditionals;
			$this->templates    = $templates;

			$conditionals = array_map(array($this, 'check_conditional_tag'), $this->conditionals);
			$templates    = array_map(array($this, 'check_page_template'), $this->templates);

			if (in_array(true, $conditionals) || in_array(true, $templates)) {
				
				$this->display = false;
			}
		
		}

		private function check_conditional_tag($conditional_tag) {
			
			if (is_array($conditional_tag)) {
				
				return call_user_func_array($conditional_tag[0], $conditional_tag[1]);
			
			} else {
				
				return $conditional_tag();
			
			}
			
		}

		private function check_page_template($page_template) {
		
			return is_page_template($page_template);
		
		}
	
	}

/* ----------------------------------------------------------------------------------
Page Title
---------------------------------------------------------------------------------- */
	
	function mhwp_page_title() {

		// Home Page Header
	    if (is_home()) {
		
		    if (get_option('page_for_posts', true)) {
		
		    	$output = apply_filters('mhwp_page_title_home_page_for_posts', get_the_title(get_option('page_for_posts', true)) );
		
		    } else {
		
			    $output = apply_filters('mhwp_page_title_home', _e('Latest Posts', 'mhwp') );
		
			}
		
		// Archive Page Header
		} elseif (is_archive()) {
			
			$term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
			
			// Term Archive Page
			if ($term) {
				
				$output = apply_filters('mhwp_page_title_term_archive', ucwords(get_post_type()).': '.$term->name );
			
			// Post Type Archive Page
			} elseif (is_post_type_archive()) {
				
				$output = apply_filters('mhwp_page_title_post_type_archive', get_queried_object()->labels->name );
				
			// Day Archive Page
			} elseif (is_day()) {
				
				$output = apply_filters('mhwp_page_title_day_archive', sprintf(__('Daily Archives: %s', 'mhwp'), get_the_date()) );
				
			// Month Archive Page
			} elseif (is_month()) {
				
				$output = apply_filters('mhwp_page_title_month_archive', sprintf(__('Monthly Archives: %s', 'mhwp'), get_the_date('F Y')) );
			
			// Year Archive Page
			} elseif (is_year()) {
				
				$output = apply_filters('mhwp_page_title_year_archive', sprintf(__('Yearly Archives: %s', 'mhwp'), get_the_date('Y')) );
			
			// Author Archive Page
			} elseif (is_author()) {
				
				global $post;
				$author_id = $post->post_author;
				$output = apply_filters('mhwp_page_title_author_archive', sprintf(__('Author Archives: %s', 'mhwp'), get_the_author_meta('display_name', $author_id)) );
			
			// All Other Pages
			} else {
				
				$output = apply_filters('mhwp_page_title_archive', single_cat_title('Archive: ',false) );
				
			}
		
		// Search Page Header
		} elseif (is_search()) {
			
			$output = apply_filters('mhwp_page_title_search', sprintf(__('Search Results for %s', 'mhwp'), get_search_query()) );
		
		// 404 Page Header
		} elseif (is_404()) {
			
			$output = apply_filters('mhwp_page_title_404', __('File Not Found', 'mhwp') );
		
		} elseif (is_singular() && get_post_type() == 'post') {
		
			$output = get_post(get_option('page_for_posts'))->post_title;
		
		} elseif (is_front_page()) {
		
			$output = '';
		
		// Anything Else
		} else {
			
			$output = apply_filters('mhwp_page_title_general', get_the_title() );
		
		}
		
		
		// Run Action Before the Page Title
		do_action('mhwp_before_page_title_text');
		
			// Output the title through a filter
			return apply_filters('mhwp_page_title', $output);
		
		// Run Action After the Page Title
		do_action('mhwp_after_page_title_text');
		
	}