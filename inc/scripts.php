<?php

/* ----------------------------------------------------------------------------------
Register Styles & Scripts
---------------------------------------------------------------------------------- */

	add_action('wp_enqueue_scripts', 'mhwp_register_scripts', 100);

	function mhwp_register_scripts() {

		if (!is_admin()) {

			// ------------------------------------ //
			// Bootstrap CSS & jQuery
			// ------------------------------------ //

				wp_register_style(
					'mhwp_bootstrap',
					get_stylesheet_directory_uri() . '/assets/css/theme-styles.css',
					false,
					false,
					'screen'

				);

				wp_register_script(
					'mhwp_bootstrap',
					get_template_directory_uri() . '/assets/js/bootstrap-plugins.js',
					array('jquery'),
					null,
					false
				);

			// ------------------------------------ //
			// FitVids jQuery
			// ------------------------------------ //

				wp_register_script(
					'mhwp_fitvids',
					get_template_directory_uri() . '/assets/js/jquery.fitvids.min.js',
					array('jquery'),
					null,
					false
				);

			// ------------------------------------ //
			// Modernizr
			// ------------------------------------ //

				wp_register_script(
					'mhwp_modernizr',
					get_template_directory_uri() . '/assets/js/modernizr.js',
					array('jquery'),
					null,
					false
				);
				
			// ------------------------------------ //
			// Transit
			// ------------------------------------ //

				wp_register_script(
					'mhwp_transit',
					get_template_directory_uri() . '/assets/js/transit.js',
					array('jquery'),
					null,
					false
				);

			// ------------------------------------ //
			// Custom JS
			// ------------------------------------ //

				wp_register_script(
					'mhwp_custom',
					get_template_directory_uri() . '/assets/js/custom.js',
					array('jquery'),
					null,
					false
				);

		} // End if !is_admin()

	}

/* ----------------------------------------------------------------------------------
Enqueue Styles & Scripts
---------------------------------------------------------------------------------- */

	add_action('wp_enqueue_scripts','mhwp_enqueue_scripts', 100);

	function mhwp_enqueue_scripts() {

		if (!is_admin()) {

			// ------------------------------------ //
			// Comment Reply
			// ------------------------------------ //
			if (is_single() && comments_open() && get_option('thread_comments')) {

				wp_enqueue_script('comment-reply');

			}

		} // End !is_admin()

		// Enqueue Standard Bootstrap Style
		wp_enqueue_style('mhwp_bootstrap');

		// Enqueue Standard Scripts
		wp_enqueue_script('mhwp_modernizr');
		wp_enqueue_script('mhwp_bootstrap');
		wp_enqueue_script('mhwp_fitvids');
		wp_enqueue_script('mhwp_transit');
		wp_enqueue_script('mhwp_custom');

	}