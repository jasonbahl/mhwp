jQuery(document).ready(function($){

/* ---------------------------------------------------------------------------- */
/* Bootstrap Tooltips
/* ---------------------------------------------------------------------------- */
	
	function v9_bootstrap_tooltips() {
	
		$("body").tooltip({
			selector: '[rel=tooltip]'	
		});
	
	}
	
	v9_bootstrap_tooltips();
	
/* ---------------------------------------------------------------------------- */
/* Fitvids - Responsive Videos
/* ---------------------------------------------------------------------------- */

	function v9_fitvids() {
		
		$('body').fitVids();
		
	}
	
	v9_fitvids();

});