<?php get_template_part('templates/head'); ?>

<body <?php body_class(); ?>>

<!--[if lt IE 7]><div class="alert">Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</div><![endif]-->
	
	<!-- Wrap the Content -->
	<div id="wrap" role="document">
		
		<?php 
		# Run Action Above the header
		do_action('mhwp_before_header'); 
		?>
		
		<?php 
		# WordPress Get Header Hook
		do_action('get_header'); 
		?>
		
		<?php 
		# Header Template Part
		get_template_part('templates/header'); 
		?>

		<?php
		# Run Action Above the Wrapper
		do_action('mhwp_before_wrapper'); 
		?>

		<div class="container">

			<div class="container-inner">

				<?php 
				# Run Action Above the Content
				do_action('mhwp_before_content'); 
				?>

				<!-- Content & Fluid Row -->
				<div id="content">

					<div class="row-<fluid></fluid>">

						<?php 
						# Run Action Above the Content
						do_action('mhwp_inside_content'); 
						?>

						<!-- Main -->
						<div id="main" class="<?php echo mhwp_main_class(); ?>" role="main">

							<?php 
							# Run Action Above the Content
							do_action('mhwp_before_template_content'); 
							?>

								<?php 
								# Include the Appropriate Template
								include mhwp_template_path(); 
								?>
							
							<?php 
							# Run Action Below the Content
							do_action('mhwp_after_content'); 
							?>

						</div><!-- /main -->

						<?php 
						# If the Sidebar is used on this template
						if (mhwp_display_sidebar()) { 
						?>

							<!-- Sidebar -->
							<aside id="sidebar" class="<?php echo mhwp_sidebar_class(); ?>" role="complementary">

								<?php 
								# Run Action Above the Content
								do_action('mhwp_before_sidebar'); 
								?>

									<?php 
									# Get the Sidebar
									get_template_part('templates/sidebar'); 
									?>

								<?php 
								# Run Action Above the Content
								do_action('mhwp_after_sidebar'); 
								?>

							</aside><!-- /sidebar -->

						<?php 
						} # End If Sidebar
						?>

					</div><!-- /content row -->

				</div><!-- /#content -->

			</div><!-- /.container-inner -->

		</div><!-- /container -->

		<?php 
		# Run action after the wrapper
		do_action('mhwp_after_wrapper'); 
		?>

			<?php 
			# Get the Footer
			get_template_part('templates/footer'); 
			?>

		<?php 
		# Run Action Below the Footer
		do_action('mhwp_after_footer'); 
		?>

	</div><!-- /#wrap -->

	<?php 
	# Footer Hook
	wp_footer(); 
	?>

</body>
</html>