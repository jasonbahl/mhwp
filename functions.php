<?php

/* ---------------------------------------------------------------------------- */
/* Set Up Constants
/* ---------------------------------------------------------------------------- */

	# NOTE: MHWP = MileHighWP
	
	# Get URL to Child Theme Directory
	define('MHWP', dirname( get_bloginfo('template_url')));
	
	# Custom Post Type Icons URL
	define('MHWP_CPT_ICONS', get_bloginfo('template_directory') . '/assets/img/cpt-icons/');

	# Path to Child Theme Directory
	define('MHWP_PATH', TEMPLATEPATH );
	
	# Re-define meta box path and URL
	define( 'MHWP_METABOXES_URL', trailingslashit( MHWP . '/inc/class-riwls-metabox' ) );
	define( 'MHWP_METABOXES_DIR', trailingslashit( MHWP_PATH . '/inc/class-riwls-metabox' ) );
	
	# Post Excerpt Length (Words)
	define('POST_EXCERPT_LENGTH', 50);
	
	# Theme Name
	$get_theme_name = explode('/themes/', get_template_directory());
	
	# Helpers
	define('THEME_NAME',                next($get_theme_name));
	define('RELATIVE_PLUGIN_PATH',      str_replace(site_url() . '/', '', plugins_url()));
	define('FULL_RELATIVE_PLUGIN_PATH', WP_BASE . '/' . RELATIVE_PLUGIN_PATH);
	define('RELATIVE_CONTENT_PATH',     str_replace(site_url() . '/', '', content_url()));
	define('THEME_PATH',                RELATIVE_CONTENT_PATH . '/themes/' . THEME_NAME);
	
	# URL to Plugin Folder
	$mhwp = get_stylesheet_directory();
	global $mhwp;

	# Gets options from the plugin options table
	$mhwp_options = get_option('mhwp');
	global $mhwp_options;

/* ---------------------------------------------------------------------------- */
/* Require Files
/* ---------------------------------------------------------------------------- */
	
	# Setup (functions that help set things up
	require_once(MHWP_PATH . '/inc/setup.php');
	
	# Handle Enqueueing Scripts
	require_once(MHWP_PATH . '/inc/scripts.php');

	# Config
	require_once(MHWP_PATH . '/inc/config.php');
	
	# NHP Options Panel
	require_once(MHWP_PATH . '/inc/class-theme-options/options-config.php');
	
	# MetaBox Class (https://github.com/rilwis/meta-box) 
	require_once(MHWP_METABOXES_DIR . 'meta-box.php');
	
	# General Cleanup
	require_once(MHWP_PATH . '/inc/cleanup.php');
	
	# Image Functions
	require_once(MHWP_PATH . '/inc/images.php');
	
	# Comment Modifications
	require_once(MHWP_PATH . '/inc/comments.php');
	
	# Pagination
	require_once(MHWP_PATH . '/inc/pagination.php');
	
	# Widgets
	require_once(MHWP_PATH . '/inc/widgets.php');