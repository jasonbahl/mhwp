<div class="row entryMeta">

	<div class="col-3">

		<!-- Show the Time -->
		<time class="updated" datetime="<?php echo get_the_time('c'); ?>" pubdate>
			<?php echo sprintf(__('%s', 'mhwp'), get_the_date()); ?>
		</time>

	</div><!-- /col-2 -->

	<div class="col-3">

		<!-- Show the Date -->
		<p class="byline author vcard">
			<?php echo __('By ', 'mhwp'); ?> <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" rel="author" class="fn"><?php echo get_the_author(); ?></a>
		</p>

	</div><!-- /col-6 -->

</div>
