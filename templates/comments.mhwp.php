<?php

// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if ( post_password_required() ) { ?>
		<p class="nocomments"><?php _e('This post is password protected. Enter the password to view comments.', 'mhwp') ?></p>
	<?php
		return;
	}

/*-----------------------------------------------------------------------------------*/
/*	Display the comments + Pings
/*-----------------------------------------------------------------------------------*/

		if ( have_comments() ) : // if there are comments ?>

        <div id="comment-wrap" class="clearfix">
	        <div class="comments-sidebar">
	
				<h3 id="comments"><?php comments_number(__('No Comments', 'mhwp'), __('One Comment', 'mhwp'), __('% Comments', 'mhwp')); ?></h3>
	
	            <p><?php // echo get_option('mhwp_comment_description'); ?>
	            <a href="#respond"><span class="continue-reading "><?php comment_form_title( __('Leave a Comment', 'mhwp'), __('Leave a Reply to %s', 'mhwp') ); ?></span></a></p>
	
			</div>
	
	        <?php 
	        $comments_by_type = &separate_comments($comments);
	        if ( ! empty($comments_by_type['comment']) ) : // if there are normal comments 
	        ?>
	
			<ol class="commentlist">
	        <?php wp_list_comments('type=comment&avatar_size=30&callback=mhwp_comment'); ?>
	        </ol>
	
	        <?php endif; ?>
	
	        <?php if ( ! empty($comments_by_type['pings']) ) : // if there are pings ?>
	
			<h3 id="pings"><?php _e('Trackbacks for this post', 'mhwp') ?></h3>
	
			<ol class="pinglist">
	        <?php wp_list_comments('type=pings&callback=mhwp_list_pings'); ?>
	        </ol>
	
	        <?php endif; ?>
	
			<div class="navigation">
				<div class="alignleft"><?php previous_comments_link(); ?></div>
				<div class="alignright"><?php next_comments_link(); ?></div>
			</div>
		</div>
		<?php
				
/*-----------------------------------------------------------------------------------*/
/*	Deal with no comments or closed comments
/*-----------------------------------------------------------------------------------*/
		
		if ('closed' == $post->comment_status ) : // if the post has comments but comments are now closed ?>
		
		<p class="nocomments"><?php _e('Comments are now closed for this article.', 'mhwp') ?></p>
		
		<?php endif; ?>

 		<?php else :  ?>
		
        <?php if ('open' == $post->comment_status) : // if comments are open but no comments so far ?>

        <?php else : // if comments are closed ?>
		
		<!--
		
		Don't Show Anything if Comments are Closed
		
		<?php if (is_single()) { ?><p class="nocomments"><?php _e('Comments are closed.', 'mhwp') ?></p><?php } ?>
		
		-->
		
		
        <?php endif; ?>
        
<?php endif;


/*-----------------------------------------------------------------------------------*/
/*	Comment Form
/*-----------------------------------------------------------------------------------*/

	if ( comments_open() ) : ?>
    
    <div id="respond-wrap" class="clearfix">
    
    <div class="comments-sidebar">
    
    	<h3><?php comment_form_title( __('Leave a Comment', 'mhwp'), __('Leave a Reply to %s', 'mhwp') ); ?></h3>
    	<p><?php echo stripslashes(get_option('mhwp_respond_description')); ?></p>
        
    </div>

	<div id="respond" class="clearfix">

	
		<div class="cancel-comment-reply">
			<?php cancel_comment_reply_link(); ?>
		</div>
	
		<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
		<p><?php printf(__('You must be %1$slogged in%2$s to post a comment.', 'mhwp'), '<a href="'.get_option('siteurl').'/wp-login.php?redirect_to='.urlencode(get_permalink()).'">', '</a>') ?></p>
		<?php else : ?>
	
		<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
			
			<?php if ( is_user_logged_in() ) : ?>
			
				<div class="row">
				
					<p><?php printf(__('Logged in as %1$s. %2$sLog out &raquo;%3$s', 'mhwp'), '<a href="'.get_option('siteurl').'/wp-admin/profile.php">'.$user_identity.'</a>', '<a href="'.(function_exists('wp_logout_url') ? wp_logout_url(get_permalink()) : get_option('siteurl').'/wp-login.php?action=logout" title="').'" title="'.__('Log out of this account', 'mhwp').'">', '</a>') ?></p>
		
				</div>
		
			<?php else : ?>				
				
				<div class="row">
					
					<div class="col-4">
					
						<!-- Name -->
						<input type="text" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" tabindex="1" />
						<label for="url"><small><?php _e('Name', 'mhwp') ?><?php if ($req) _e("*", 'mhwp'); ?></small></label>
	
					</div>
					
					<div class="col-4">
					
						<!-- Email -->
						<input type="text" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" tabindex="2" />
						<label for="email"><small><?php _e('Email', 'mhwp') ?><span> <?php if ($req) _e("*", 'mhwp'); ?></span> <span class="grey"><?php _e('(never published)', 'mhwp'); ?></span> </small></label>
						
					</div>
					
					<div class="col-4">
					
						<!-- Website -->
						<input type="text" name="url" id="url" value="<?php echo esc_attr($comment_author_url); ?>" tabindex="3" />
						<label for="url"><small><?php _e('Website', 'mhwp') ?></small></label>
	
					</div>
	
				</div>
				
			<?php endif; ?>
					
			<div class="row">
			
				<textarea name="comment" id="comment" rows="5" tabindex="4" class="col-12"></textarea>
				
			</div>
			
			<!--<p class="allowed-tags"><small><strong>XHTML:</strong> You can use these tags: <code><?php echo allowed_tags(); ?></code></small></p>-->
			
			<div class="row">
			
				<div class="col-12">
				
					<input name="submit" type="submit" id="submit" class="btn btn-primary col-4 pull-right" tabindex="5" value="<?php _e('Submit Comment', 'mhwp') ?>" />
				
					<?php comment_id_fields(); ?>
				
				</div>
			
			</div>
			
			<?php do_action('comment_form', $post->ID); ?>
	
		</form>

	<?php endif; // If registration required and not logged in ?>
	</div>
	</div>
	<?php endif; // if you delete this the sky will fall on your head ?>
