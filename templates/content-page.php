<?php while (have_posts()) : the_post(); ?>
	
	<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
	
  		<!-- The Content -->
  		<?php the_content(); ?>
  		
	</article>

  <?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>

<?php endwhile; ?>