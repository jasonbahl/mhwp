<?php while (have_posts()) : the_post(); ?>

	<?php
	// Run Action Before Start of Article
	do_action('mhwp_before_single_article');
	?>
	
	<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

		<?php
		// Run Action Before Article Header
		do_action('mhwp_before_single_article_header');
		?>

		<header>

			<?php
			// Run Action Before Single Article Title
			do_action('mhwp_before_single_article_title');
			?>

				<!-- Single Article Title -->
				<h1 class="entry-title">
					<?php
					// Output Title Text Through a Filter
					echo apply_filters('mhwp_single_article_title_text', get_the_title(), get_the_ID() );
					?>
				</h1>

			<?php
			// Run Action After Single Article Title
			do_action('mhwp_after_single_article_title');
			?>

			<!-- Get Entry Meta -->
			<?php get_template_part('templates/entry-meta'); ?>

		</header><!-- /header -->

		<?php
		// Run Action After Article Header
		do_action('mhwp_after_single_article_header');
		?>

		<?php
		// Run Action Before Single Content
		do_action('mhwp_before_single_content');
		?>
		<div class="entry-content">

			<!-- Output the Content -->
			<?php the_content(); ?>

		</div>

		<?php
		// Run Action After Single Content
		do_action('mhwp_after_single_content');
		?>

		<?php
		// Run Action Before Single Content Footer
		do_action('mhwp_before_single_article_footer');
		?>

		<footer>

			<?php
			// Run Action Before Single Content Footer Content
			do_action('mhwp_before_single_article_footer_content');
			?>

			<?php
			// Link Pages for Multi-Page posts
			wp_link_pages();
			?>

			<?php
			// Output Post Tags
			apply_filters('mhwp_single_content_tags', the_tags( '<div class="single-post-tags"><div class="single-post-tags-inner"><p>Tags: ', ', ', '</p></div></div>'), get_the_ID() );
			?>

			<?php
			// Run Action After Single Content Footer Content
			do_action('mhwp_after_single_article_footer_content');
			?>

		</footer>

		<?php
		// Run Action After Single Content Footer
		do_action('mhwp_after_single_article_footer');
		?>

	</article>

	<!-- Comments Template -->
	<?php comments_template('/templates/comments.php'); ?>

	<?php
	// Run Action After Close of Article
	do_action('mhwp_after_single_article');
	?>

<?php endwhile; ?>