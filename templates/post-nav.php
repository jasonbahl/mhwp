<div class="entryMeta post-nav">
	
	<div class="row">							

		<?php 
		global $post;
		
		$obj = get_post_type_object( get_post_type() );
		$next_post = get_next_post();
		$previous_post = get_previous_post();
		
		?>
		
		<!-- Previous -->
		<?php if ($previous_post) { ?>
			<a href="<?php echo get_permalink($previous_post->ID); ?>" class="postNav" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Previous <?php echo ucwords($obj->labels->singular_name); ?>"><i class="icon-chevron-left"></i></a>
		<?php } ?>
		
		<!-- All -->
		<a href="<?php echo get_post_type_archive_link( get_post_type() ); ?>" class="postNav" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="All <?php echo ucwords($obj->labels->name); ?>"><i class="icon-th"></i></a>
		
		<!-- Next -->
		<?php if ($next_post) { ?>
			<a href="<?php echo get_permalink($next_post->ID); ?>" class="postNav" rel="tooltip" data-toggle="tooltip" data-placement="top" data-original-title="Next <?php echo ucwords($obj->labels->singular_name); ?>"><i class="icon-chevron-right"></i></a>
		<?php } ?>
	
	</div>

</div>